// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TopDawnShootGameMode.h"
#include "TopDawnShootPlayerController.h"
#include "Character/TopDawnShootCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATopDawnShootGameMode::ATopDawnShootGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATopDawnShootPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}