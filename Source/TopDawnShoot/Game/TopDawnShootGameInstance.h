// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/gameInstance.h"
#include "FuncLibrary/Types.h"
#include "Character/WeaponDefault.h"
#include "Engine/DataTable.h"
#include "TopDawnShootGameInstance.generated.h"


/**
 * 
 */
UCLASS()
class TOPDAWNSHOOT_API UTopDawnShootGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
	UDataTable* WeaponInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
};
