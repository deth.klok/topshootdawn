// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TopDawnShoot.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TopDawnShoot, "TopDawnShoot" );

DEFINE_LOG_CATEGORY(LogTopDawnShoot)
 