// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/ArrowComponent.h"
#include "GameFramework/Actor.h"
#include "WorldItemDefault.generated.h"

UCLASS()
class TOPDAWNSHOOT_API AWorldItemDefault : public AActor
{
	GENERATED_BODY()

	UPROPERTY()
	USceneComponent* SceneComponent;
	UPROPERTY()
	USkeletalMeshComponent* SkeletalMeshWeapon;

	UPROPERTY()
	UArrowComponent* ShootLocation;

	
	
	
	
public:	
	// Sets default values for this actor's properties
	AWorldItemDefault();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
